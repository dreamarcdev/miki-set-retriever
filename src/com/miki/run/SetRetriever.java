/**
 * 
 */
package com.miki.run;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.Certificate;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.miki.run.intf.ConfigureInfo;
import com.miki.run.intf.RetrieverIntf;

/**
 * @author vorachate
 *
 */
public class SetRetriever extends TimerTask implements RetrieverIntf, ConfigureInfo {
	
	@Override
	public void run() {
		System.out.println("Run on : " + new Date());
		execute();
		update();
	}
	
	 
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
//		exec.readFromFile();
	}

	public void readFromFile() {
		try {
			
			File initialFile = new File("data.out");
			InputStream targetStream = new FileInputStream(initialFile);

			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.parse(targetStream);
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();

			XPathExpression expr = xpath.compile("//*[@id=\"maincontent\"]/div/div[1]/div/div/table/tbody/tr[1]/td[2]");
			Object results = expr.evaluate(document, XPathConstants.NODESET);
			NodeList nodes = (NodeList) results;
			System.out.println(nodes.getLength());

			for (int i = 0; i < nodes.getLength(); i++) {
				System.out.println(nodes.item(i).getNodeValue());
			}
		} catch (IOException | ParserConfigurationException | SAXException | XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	@Override
	public void execute() {

		int responseCode = 0;

		try {
			URL url = new URL(API);

			HttpsURLConnection httpsConnection = (HttpsURLConnection) url.openConnection();

			// dumpl all cert info
			// printHttpsCert(httpsConnection);

			// dump all the content
			// printContent(httpsConnection);

			httpsConnection.connect();
			responseCode = httpsConnection.getResponseCode();
			if (responseCode == 200) {
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

				Document document = builder.parse(httpsConnection.getInputStream());
				XPathFactory xPathfactory = XPathFactory.newInstance();
				XPath xpath = xPathfactory.newXPath();

				XPathExpression expr = xpath
						.compile("//*[@id=\"maincontent\"]/div/div[1]/div/div/table/tbody/tr[1]/td[2]");
				Object results = expr.evaluate(document, XPathConstants.NODESET);
				NodeList nodes = (NodeList) results;
				System.out.println(nodes.getLength());

				for (int i = 0; i < nodes.getLength(); i++) {
					System.out.println(nodes.item(i).getNodeValue());
				}

				expr = xpath.compile("//geometry/location/lng");
				String lng = (String) expr.evaluate(document, XPathConstants.STRING);
				System.out.println(lng);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void printHttpsCert(HttpsURLConnection con) {

		if (con != null) {

			try {

				System.out.println("Response Code : " + con.getResponseCode());
				System.out.println("Cipher Suite : " + con.getCipherSuite());
				System.out.println("\n");

				Certificate[] certs = con.getServerCertificates();
				for (Certificate cert : certs) {
					System.out.println("Cert Type : " + cert.getType());
					System.out.println("Cert Hash Code : " + cert.hashCode());
					System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
					System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
					System.out.println("\n");
				}

			} catch (SSLPeerUnverifiedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	@Override
	public void printContent(HttpsURLConnection con) {
		final Path dst = Paths.get("out.txt");
		final BufferedReader br;
		final BufferedWriter writer;
		String line;
		if (con != null) {

			try {
				writer = Files.newBufferedWriter(dst, StandardCharsets.UTF_8);
				System.out.println("****** Content of the URL ********");
				br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((line = br.readLine()) != null) {
					writer.write(line);
					writer.newLine();
				}

				br.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void retrieveContent() {
		// TODO Auto-generated method stub

	}


	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
