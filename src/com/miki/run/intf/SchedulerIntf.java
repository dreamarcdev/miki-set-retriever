/**
 * 
 */
package com.miki.run.intf;

import java.util.concurrent.TimeUnit;

/**
 * @author vorachate
 *
 */
public interface SchedulerIntf {
	public void setTime(long start , long period, TimeUnit unit);
	public void run();
}
