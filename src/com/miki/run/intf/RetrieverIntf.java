/**
 * 
 */
package com.miki.run.intf;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author vorachate
 *
 */
public interface RetrieverIntf {
	public void execute();
	
	public void retrieveContent();
	
	public void update();
	
	public void printHttpsCert(HttpsURLConnection con);
	public void printContent(HttpsURLConnection con);
}
