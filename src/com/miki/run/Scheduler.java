/**
 * 
 */
package com.miki.run;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.miki.run.intf.SchedulerIntf;

/**
 * @author vorachate
 *
 */
public class Scheduler implements SchedulerIntf {
	
	private long start = 0;
	private long period = 5;
	private TimeUnit unit = TimeUnit.MINUTES;
	
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scheduler r = new Scheduler();
		r.run();

	}

	@Override
	public void setTime(long start , long period, TimeUnit unit) {
		this.start = start;
		this.period = period;
		this.unit = unit;
	}

	@Override
	public void run() {
		SetRetriever runner = new SetRetriever();
		scheduler.scheduleAtFixedRate(runner, start, period, unit);
		
	}


}
